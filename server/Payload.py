class Payload(object):
    TYPE_TEXT = 0
    TYPE_BINARY = 1

    def __init__(self, file_path, data=None):
        
        if data:
            self.payload = data
            self._type = Payload.TYPE_TEXT
        elif file_path:
            self._set_payload_type(file_path)
            self._open_file(file_path)
        else:
            print("Veri yok")

    def _open_file(self, path):
        
        f_type = 'r'
        if self._type == self.TYPE_BINARY:
            f_type += "b"

        with open(path, f_type) as f:
            self.payload = f.read()

    def _set_payload_type(self, path):
        text_files = ("txt", "html", "sh", "sena")
        self.ext = path.split('.')[-1].strip()
        
        if self.ext in text_files:
            self._type = Payload.TYPE_TEXT
        else:
            self._type = Payload.TYPE_BINARY
            
    def serialize_byte(self):
        if self._type == Payload.TYPE_TEXT:
            return self.payload.encode("UTF-8")

        return self.payload

    def get_type(self):
        return self._type

    def get_size(self):
        return len(self.payload)


if __name__ == "__main__":
    Pl = Payload(file_path="./files/did.sena")
    print(type(Pl.serialize_byte()))

    

import Request.Request as Request
import Response.R_Created as CRTD
import Response.R_Teapot as Teapot
import Payload


class Put(Request.Request):
    def __init__(self, message):
        super().__init__(message)

    def request_type(self):
        return "put"

    def generate_response(self):
        check = super().generate_response()

        if not check:
            return Teapot.Teapot()


        ## Todo Payloada ekle
        try:
            with open(self.header["path"], "wb") as f:
                f.write(self.raw_body.encode("UTF-8"))
            
            return CRTD.Created()
        except Exception as e:
            print(e)
            import Response.R_Bad_Request as bdrqst
            return bdrqst.Bad_Request()

        

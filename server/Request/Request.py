class Request(object):
    
    def __init__(self, message):
        self.raw_header, self.raw_body = self.div_message(message.decode("UTF-8"))
        self._parse_request()

        #self.response = self.generate_response()
        #print(f"{self.request_type}")
        
    @property
    def request_type(self):
        raise NotImplementedError("Belrtilmeyen Tip")
    
    def generate_response(self):
        import os.path

        if not os.path.isfile(self.header["path"]):
            import Response.R_Not_Found as NotFound
            return NotFound.NotFound()
        

    def div_message(self, message):
        return  message.split("\r\n\r\n", 2)
        
    def _parse_request(self):
        self.header = {}
        
        splitted = [i.strip() for i in self.raw_header.split("\n")]
        first_line = splitted[0].split()

        self.header["method"] = first_line[0]
        self.header["path"] = "." + first_line[1]
        self.header["protocol"] = first_line[2]

        del splitted[0]

        for i in splitted:
            header_line = i.split(":")
            self.header[header_line[0].strip()] = header_line[1]

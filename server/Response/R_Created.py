from Response import Response
import Payload 

class Created(Response.Response):
    def __init__(self):
        payload = Payload.Payload("./static/Created.html")
        super().__init__(payload)

    def get_type(self):
        return "CREATED"

    def get_request_code(self):
        return 201

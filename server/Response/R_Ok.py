import Response.Response as response

class Ok(response.Response):
    def __init__(self, payload):
        super().__init__(payload)

    def get_type(self):
        return "OK"

    def get_request_code(self):
        return 200


if __name__ == "__main__":
    r = Ok()
    print(r.generate_header())
        

from Response import Response
import Payload

class BadRequest(Response.Response):
    def __init__(self):
        payload = Payload.Payload("./static/BadRequest.html") 
        super().__init__(payload)

    def get_type(self):
        return "Bad Request"

    def get_request_code(self):
        return 400

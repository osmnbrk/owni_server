from Response import Response
import Payload

class NotFound(Response.Response):
    def __init__(self):
        payload = Payload.Payload("./static/NotFound.html")
        super().__init__(payload)

    def get_type(self):
        return "Not Found"

    def get_request_code(self):
        return 404

from Response import Response
import Payload

class Teapot(Response.Response):
    def __init__(self):
        payload = Payload.Payload("./static/Sena.html")
        super().__init__(payload)

    def get_type(self):
        return "I'm a teapot"

    def get_request_code(self):
        return 418

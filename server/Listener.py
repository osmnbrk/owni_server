"""
Sistemi ayaga kaldiracak ana dongunun bukundugu surekli calisacak siniftir
gerektiginde asyncio ya da thread kutuphanesiyle guclendirilebilir. 
"""

import socket, datetime
import Request.RequestManager as RequestManager
import time

class ApiListener(object):
    def __init__(self, family=socket.AF_INET, _type=socket.SOCK_STREAM):
        self._loop_flag = False
        self._socket_bound = False
        self._recv_size = 512
        
        self._socket = socket.socket(family, _type)

    def create_socket(self, host="127.0.0.1", port=3816):
        address = (host, port)
        try:
            self._socket.bind(address)
            self._socket_bound = True
            
            print(f"Api {host} adresine ve {port} portuna baglandi.")

        except Exception as error:
            print(error)
            print("Soket kapatiliyor...")
            self._socket.close()
            exit()

    def __on_request(self, connection, client, message):

        now = datetime.datetime.now().time().strftime("%H:%M.%S")
        print(f"[{now}] -- ", end='')
        print(f"{client[0]}:{client[1]} --  ", end="")

        hdr = message.decode("UTF-8").split(" ", 2)
        print(hdr[0], hdr[1])

        request = RequestManager.Manager(hdr[0])
        request = request(message)
        response = request.generate_response()
        connection.sendall(response.serialize())

        ## HTTP requestlerinde client sorgusu kapanamadan sorgu gonderildi
        ## sayilmaz. Bu yuzden her request geldiginde islemlerden sonra
        ## baglantu kapatilir. (Connection kapatilir socket degil!!!)
        connection.close()


    def run_api(self):
       if not self._socket_bound:
           raise RuntimeError("First you should call create_socket method.")

       try:
           self._socket.listen()
           self._loop_flag = True
           self.__mainloop()
           
       except Exception as error:
           self.close_api()
           print(error)
           exit()

    def __mainloop(self):
        while self._loop_flag:
            connection, address = self._socket.accept()
            connection.setblocking(0)
            message = self.recv_all(connection)
            self.__on_request(connection, address, message)
        self.close_api()
        
    def recv_all(self, connection, timeout=2):
        
        connection.settimeout(1) 
        message = b''
        
        while True:
            try:
                data = connection.recv(1024)
                message += data
            except:
                return message   
             
        
        
    def close_api(self):
        self._socket.close()
        print("Api basariyla kapatildi")
        
    
if __name__ == "__main__":
    import sys
    
    api = ApiListener()
    api.create_socket(port=int(sys.argv[1]))
    
    api.run_api()

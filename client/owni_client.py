import socket

header= """{} {} {}\r
Host: Berk-Ay\r
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r
Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r
Pragma: no-cache\r
Cache-Control: no-cache\r\n\r
"""

def parse_response(data):
    data=data
    
    i = 0
    length = len(data)
    print(length)
    while i < length-5:
        x,y,z,t = data[i], data[i+1], data[i+2], data[i+3]
        if x == 13 and y == 10 and z == 13 and t == 10:
            break  
        i += 1
    
    # header,context=data.split("\r\n\r\n", 2)
    header = data[:i+3].decode("UTF-8")
    context = data[i+4:]
    
    first_line=header.split("\n")[0]
    print(first_line)
    response=first_line.split()[1] + " " + first_line.split()[2]

    return response, context


def recv_all(socket, timeout=2):
        
    socket.settimeout(timeout) 
    message = b''
    
    while True:
        try:
            data = socket.recv(1024)
            message += data
            print(data)
        except:
            return message

def get (ip , port , path):
    h=header.format("GET", path, "HTTP/1.1")
    s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (ip, int(port))
    s.connect(server_address)

    s.sendall(h.encode("UTF-8"))
    data = b""
    while True:
        tmp_data = s.recv(1024)
        if not tmp_data:
            break
        data += tmp_data

    code,context=parse_response(data)
    print(code)
    _path=path.split("/")[-1]
    with open(_path, "wb") as file:
        file.write(context)

    s.close()


def put(ip,port,path):
    server_path=input("Sunucu yolunu giriniz-->")
    h = header.format("PUT", server_path, "HTTP/1.1")
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (ip, int(port))
    s.connect(server_address)
    with open(path, "rb") as file:
        s.sendall(h.encode("UTF-8")+file.read())
        data=b""
        while True:
            tmp_data = s.recv(1024)
            if not tmp_data:
                break
            data += tmp_data
    code,context=parse_response(data)
    print(code)
    s.close()


def delete(ip,port,path):
    h = header.format("DELETE", path, "HTTP/1.1")
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (ip, int(port))
    s.connect(server_address)

    s.sendall(h.encode("UTF-8"))
    data=s.recv(1024)
    hdr,context=parse_response(data)
    print(hdr)

    s.close()


def main():
    ip=input("Server ipsini giriniz-->")
    port=input("Port numarasını giriniz-->")
    path=input("Dosya yolunu giriniz-->")
    while True:
        a=input("""1-GET istemi\n2-PUT istemi\n3-DELETE istemi\n4-Cikis\n\n""")
        if a == '1':
            get(ip, port, path)
        elif a == '2':
            put(ip, port, path)
        elif a == '3':
            delete(ip, port, path)
        elif a == '4':
            break


if __name__ == '__main__':
    main()
